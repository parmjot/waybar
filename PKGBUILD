# Maintainer: Brett Cornwall <ainola@archlinux.org>
# Maintainer: Maxim Baz <archlinux at maximbaz dot com>
# Contributor: Parmjot Singh <parmjotsinghrobot at gmail dot com>

pkgname=waybar
pkgver=0.10.0
pkgrel=3
pkgdesc='Highly customizable Wayland bar for Sway and Wlroots based compositors'
arch=('x86_64')
url="https://github.com/Alexays/Waybar/"
license=('MIT')
depends=(
    'fmt'
    'gtk-layer-shell'
    'gtkmm3'
    'jack' 'libjack.so'
    'libatkmm-1.6.so'
    'libcairomm-1.0.so'
    'libdbusmenu-gtk3'
    'libevdev'
    'libgtk-3.so'
    'libinput'
    'libjsoncpp.so'
    'libmpdclient'
    'libnl'
    'libpipewire-0.3.so'
    'libpulse'
    'libsigc++'
    'libsndio.so'
    'libspdlog.so'
    'libudev.so'
    'libupower-glib.so'
    'libwireplumber'
    'libxkbcommon'
    'playerctl'
    'upower'
    'wayland'
)
makedepends=(
    'cmake'
    'catch2'
    'meson'
    'scdoc' # For generating manpages
    'wayland-protocols'
)
backup=(
    etc/xdg/waybar/config.jsonc
    etc/xdg/waybar/style.css
)
optdepends=(
    'otf-font-awesome: Icons in the default configuration'
)
source=(
    "$pkgname-$pkgver.tar.gz::https://github.com/Alexays/Waybar/archive/$pkgver.tar.gz"
    0001-Update-Wireplumber-API-to-0.5.patch
)
b2sums=('f27a61662444cb0d91ecb5c860271939ba81813af4d005a3297f9996fab5edd54df2e4a126aa33edf9049e2d91d301a03f6352e25eb9c86ffb003b0b238e9015'
        'bc8b314fc2ca29a47faa766eeb7837a21d3fc75769942ab34c3d78ba6c0ae98090ecdd1b1743af91dcefed377afe002b1f4267ecae7804b3d367cb84acaaa8ce'
)

prepare() {
    cd "Waybar-$pkgver"

    # https://github.com/Alexays/Waybar/commit/9d95eaaac41c3286a6a400275d0da3eb0ba52c43
    patch -Np1 -i ../0001-Update-Wireplumber-API-to-0.5.patch
}

build() {
    cd "Waybar-$pkgver"
    # TODO tests depend on catch2 v3
    CXXFLAGS+=" -std=c++20" \
    meson --prefix=/usr \
          --buildtype=plain \
          --auto-features=enabled \
          --wrap-mode=nodownload \
          -Dexperimental=true \
          -Dcava=disabled \
          -Dtests=enabled \
          build
    ninja -C build
}

check() {
    cd "Waybar-$pkgver"
    meson test -C build --no-rebuild --suite waybar

}

package() {
    cd "Waybar-$pkgver"
    DESTDIR="$pkgdir" ninja -C build install
    install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname/"
}
